"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_1 = require("tns-core-modules/platform");
var dialogs = require("tns-core-modules/ui/dialogs");
var application_1 = require("tns-core-modules/application");
var utils_1 = require("tns-core-modules/utils/utils");
var getApplication = utils_1.ad.getApplication;
var HomeComponent = /** @class */ (function () {
    function HomeComponent() {
    }
    HomeComponent.prototype.openDialog = function () {
        dialogs.alert({
            title: "Example",
            message: "You can use everything you would normally use in Angular and Android application. Things start to get a little bit different when you want use some specific API. But, this is a lot easier because you don't have to write Swift code for iOs and Kotlin code for Android. You just write two methods using TypeScript and determine which to call during runtime depending on OS.",
            okButtonText: "Close dialog"
        }).then(function () {
            console.log("Dialog closed!");
        });
    };
    HomeComponent.prototype.ngOnInit = function () {
        this.osType = getApplication();
        console.log(typeof this.osType);
        console.log(application_1.ios);
        this.context = application_1.android.context;
        console.error("Android context:" + this.context);
        this.wifiManager = application_1.android.context.getSystemService('wifi');
        this.wInfo = this.wifiManager.getConnectionInfo();
        this.mac = this.wInfo.getMacAddress();
        this.uid = platform_1.device.uuid;
        console.log(platform_1.device.uuid);
    };
    HomeComponent = __decorate([
        core_1.Component({
            selector: "Home",
            moduleId: module.id,
            templateUrl: "./home.component.html"
        }),
        __metadata("design:paramtypes", [])
    ], HomeComponent);
    return HomeComponent;
}());
exports.HomeComponent = HomeComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaG9tZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJob21lLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFnRDtBQUNoRCxzREFBaUQ7QUFDakQscURBQXVEO0FBQ3ZELDREQUEwRDtBQUMxRCxzREFBZ0Q7QUFDaEQsSUFBTyxjQUFjLEdBQUcsVUFBRSxDQUFDLGNBQWMsQ0FBQztBQU8xQztJQVFJO0lBRUEsQ0FBQztJQUVELGtDQUFVLEdBQVY7UUFDSSxPQUFPLENBQUMsS0FBSyxDQUFDO1lBQ1YsS0FBSyxFQUFFLFNBQVM7WUFDaEIsT0FBTyxFQUFFLHFYQUFxWDtZQUM5WCxZQUFZLEVBQUUsY0FBYztTQUMvQixDQUFDLENBQUMsSUFBSSxDQUFDO1lBQ0osT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1FBQ2xDLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELGdDQUFRLEdBQVI7UUFDSSxJQUFJLENBQUMsTUFBTSxHQUFHLGNBQWMsRUFBRSxDQUFDO1FBQy9CLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDaEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBRyxDQUFDLENBQUM7UUFFakIsSUFBSSxDQUFDLE9BQU8sR0FBRyxxQkFBTyxDQUFDLE9BQU8sQ0FBQztRQUMvQixPQUFPLENBQUMsS0FBSyxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNqRCxJQUFJLENBQUMsV0FBVyxHQUFHLHFCQUFPLENBQUMsT0FBTyxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzVELElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1FBQ2xELElBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUV0QyxJQUFJLENBQUMsR0FBRyxHQUFHLGlCQUFNLENBQUMsSUFBSSxDQUFDO1FBRXZCLE9BQU8sQ0FBQyxHQUFHLENBQUMsaUJBQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUM3QixDQUFDO0lBcENRLGFBQWE7UUFMekIsZ0JBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxNQUFNO1lBQ2hCLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixXQUFXLEVBQUUsdUJBQXVCO1NBQ3ZDLENBQUM7O09BQ1csYUFBYSxDQXFDekI7SUFBRCxvQkFBQztDQUFBLEFBckNELElBcUNDO0FBckNZLHNDQUFhIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIE9uSW5pdH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7ZGV2aWNlfSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy9wbGF0Zm9ybVwiO1xuaW1wb3J0ICogYXMgZGlhbG9ncyBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy91aS9kaWFsb2dzXCI7XG5pbXBvcnQge2FuZHJvaWQsIGlvc30gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvYXBwbGljYXRpb25cIjtcbmltcG9ydCB7YWR9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL3V0aWxzL3V0aWxzXCI7XG5pbXBvcnQgZ2V0QXBwbGljYXRpb24gPSBhZC5nZXRBcHBsaWNhdGlvbjtcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6IFwiSG9tZVwiLFxuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWQsXG4gICAgdGVtcGxhdGVVcmw6IFwiLi9ob21lLmNvbXBvbmVudC5odG1sXCJcbn0pXG5leHBvcnQgY2xhc3MgSG9tZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gICAgb3NUeXBlOiBhbnk7XG4gICAgY29udGV4dDogYW55O1xuICAgIHdpZmlNYW5hZ2VyOiBhbnk7XG4gICAgd0luZm86IGFueTtcbiAgICBtYWM6IGFueTtcbiAgICB1aWQ6IGFueTtcblxuICAgIGNvbnN0cnVjdG9yKCkge1xuXG4gICAgfVxuXG4gICAgb3BlbkRpYWxvZygpIHtcbiAgICAgICAgZGlhbG9ncy5hbGVydCh7XG4gICAgICAgICAgICB0aXRsZTogXCJFeGFtcGxlXCIsXG4gICAgICAgICAgICBtZXNzYWdlOiBgWW91IGNhbiB1c2UgZXZlcnl0aGluZyB5b3Ugd291bGQgbm9ybWFsbHkgdXNlIGluIEFuZ3VsYXIgYW5kIEFuZHJvaWQgYXBwbGljYXRpb24uIFRoaW5ncyBzdGFydCB0byBnZXQgYSBsaXR0bGUgYml0IGRpZmZlcmVudCB3aGVuIHlvdSB3YW50IHVzZSBzb21lIHNwZWNpZmljIEFQSS4gQnV0LCB0aGlzIGlzIGEgbG90IGVhc2llciBiZWNhdXNlIHlvdSBkb24ndCBoYXZlIHRvIHdyaXRlIFN3aWZ0IGNvZGUgZm9yIGlPcyBhbmQgS290bGluIGNvZGUgZm9yIEFuZHJvaWQuIFlvdSBqdXN0IHdyaXRlIHR3byBtZXRob2RzIHVzaW5nIFR5cGVTY3JpcHQgYW5kIGRldGVybWluZSB3aGljaCB0byBjYWxsIGR1cmluZyBydW50aW1lIGRlcGVuZGluZyBvbiBPUy5gLFxuICAgICAgICAgICAgb2tCdXR0b25UZXh0OiBcIkNsb3NlIGRpYWxvZ1wiXG4gICAgICAgIH0pLnRoZW4oKCkgPT4ge1xuICAgICAgICAgICAgY29uc29sZS5sb2coXCJEaWFsb2cgY2xvc2VkIVwiKTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgbmdPbkluaXQoKTogdm9pZCB7XG4gICAgICAgIHRoaXMub3NUeXBlID0gZ2V0QXBwbGljYXRpb24oKTtcbiAgICAgICAgY29uc29sZS5sb2codHlwZW9mIHRoaXMub3NUeXBlKTtcbiAgICAgICAgY29uc29sZS5sb2coaW9zKTtcblxuICAgICAgICB0aGlzLmNvbnRleHQgPSBhbmRyb2lkLmNvbnRleHQ7XG4gICAgICAgIGNvbnNvbGUuZXJyb3IoXCJBbmRyb2lkIGNvbnRleHQ6XCIgKyB0aGlzLmNvbnRleHQpO1xuICAgICAgICB0aGlzLndpZmlNYW5hZ2VyID0gYW5kcm9pZC5jb250ZXh0LmdldFN5c3RlbVNlcnZpY2UoJ3dpZmknKTtcbiAgICAgICAgdGhpcy53SW5mbyA9IHRoaXMud2lmaU1hbmFnZXIuZ2V0Q29ubmVjdGlvbkluZm8oKTtcbiAgICAgICAgdGhpcy5tYWMgPSB0aGlzLndJbmZvLmdldE1hY0FkZHJlc3MoKTtcblxuICAgICAgICB0aGlzLnVpZCA9IGRldmljZS51dWlkO1xuXG4gICAgICAgIGNvbnNvbGUubG9nKGRldmljZS51dWlkKTtcbiAgICB9XG59XG4iXX0=